"------------------------------------
" Load default settings (for Linux)
"------------------------------------
" ~r-yamada/dotfiles/dot.vimrc.default$B$,B8:_$9$k>l9g$N$_@_Dj$rFI$_9~$`(B
let s:OSTYPE = system('uname')
if s:OSTYPE == "Linux\n" || s:OSTYPE == "SunOS\n"
  let s:default_vimrc = expand('~r-yamada/dotfiles/dot.vimrc.default')
  if filereadable(s:default_vimrc)
    execute 'source ' . s:default_vimrc
  endif
endif

"------------------------------------
" someting variables
"------------------------------------
" $B9THV9f$NI=<((B
set number
" $B%+!<%=%k9T$r%O%$%i%$%H(B
set cursorline
" $B%^%k%A%P%$%HJ8;z$rI=<((B
set ambiwidth=double
" $BJQ99Cf$G$b!$B>$N%U%!%$%k$rJQ99$G$-$k$h$&$K(B
set hidden
" $B8!:w7k2L$r%O%$%i%$%H(B
set hlsearch
" $B>o$K%9%F!<%?%99T$rI=<((B
set laststatus=2
" $B%3%^%s%I%i%$%s$N9b$5(B
set cmdheight=2
" $B%?%$%H%k$rI=<((B
set title
" $B%3%^%s%I$r%9%F!<%?%99T$KI=<((B
set showcmd
" $B%P%C%/%9%Z!<%9$G%$%s%G%s%H$d2~9T$r:o=|$G$-$k$h$&$K$9$k(B
set backspace=indent,eol,start
" $B8!:w;~$K%U%!%$%k$N:G8e$^$G9T$C$?$i:G=i$KLa$k(B (nowrapscan:$BLa$i$J$$(B)
set wrapscan
" $B3g8LF~NO;~$KBP1~$9$k3g8L$rI=<((B (noshowmatch:$BI=<($7$J$$(B)
set showmatch
" $B%3%^%s%I%i%$%sJd40$9$k$H$-$K6/2=$5$l$?$b$N$r;H$&(B($B;2>H(B :help wildmenu)
set wildmenu
" $B%F%-%9%HA^F~Cf$N<+F0@^$jJV$7$rF|K\8l$KBP1~$5$;$k(B
set formatoptions+=mM
" $B8!:w;~$KBgJ8;z>.J8;z$rL5;k(B (noignorecase:$BL5;k$7$J$$(B)
set ignorecase
set tw=0 
set incsearch
" $BMzNrJ]B8?t(B
set history=100

if !has('gui_running') && has('xterm_clipboard')
  set clipboard=exclude:cons\\\|linux\\\|cygwin\\\|rxvt\\\|screen
endif

"------------------------------------
" NeoBundle
"------------------------------------
set nocompatible               " be iMproved
filetype off

if has('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim
    " call neobundle#rc(expand('~/.vim/bundle/'))
    call neobundle#begin(expand('~/.vim/bundle/'))
endif
" originalrepos on github
" NeoBundle 'YankRing.vim'
NeoBundle 'LeafCage/yankround.vim'
NeoBundle 'itchyny/landscape.vim'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'tomasr/molokai'
NeoBundle 'croaker/mustang-vim'
NeoBundle 'nanotech/jellybeans.vim'
NeoBundle 'vim-scripts/Lucius'
NeoBundle 'vim-scripts/Zenburn'
NeoBundle 'mrkn/mrkn256.vim'
NeoBundle 'jpo/vim-railscasts-theme'
NeoBundle 'therubymug/vim-pyte'
NeoBundle 'Shougo/neobundle.vim'
NeoBundle 'Shougo/vimproc'
NeoBundle 'Shougo/vimshell'
NeoBundle 'VimClojure'
NeoBundle 'Shougo/unite.vim'
NeoBundle 'ujihisa/unite-colorscheme'
NeoBundle 'Shougo/neocomplcache'
NeoBundleFetch 'Shougo/neocomplete.vim'
NeoBundle 'Shougo/neosnippet'
NeoBundle 'Shougo/Neosnippet-snippets'
NeoBundleFetch 'jpalardy/vim-slime'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'tsukkee/unite-help'
NeoBundle 'h1mesuke/unite-outline'
NeoBundle 'sgur/unite-git_grep'
NeoBundle 'Shougo/unite-build'
NeoBundle 'hewes/unite-gtags'
NeoBundleFetch 'bling/vim-airline'
NeoBundleFetch 'tpope/vim-fugitive'
NeoBundle 'thinca/vim-template'
NeoBundle 'nathanaelkane/vim-indent-guides'
NeoBundle 'kana/vim-tabpagecd'
NeoBundleFetch 'thinca/vim-visualstar'
NeoBundle 'tpope/vim-surround'
NeoBundle 'bkad/CamelCaseMotion'
NeoBundle 'thinca/vim-poslist'
NeoBundle 'jceb/vim-hier'
NeoBundle 'thinca/vim-ref'
NeoBundleFetch 'basyura/TweetVim'
NeoBundle 'mattn/webapi-vim'
NeoBundle 'basyura/twibill.vim'
NeoBundle 'tyru/open-browser.vim'
NeoBundle 'basyura/bitly.vim'
NeoBundle 'git://git.code.sf.net/p/vim-latex/vim-latex'
NeoBundleFetch 'tpope/vim-pathogen'
NeoBundle 'vim-scripts/MultipleSearch'
NeoBundle 'thinca/vim-quickrun'
NeoBundle 'hrp/EnhancedCommentify'

NeoBundle 'troydm/easybuffer.vim'
NeoBundle 'osyo-manga/vim-over'
NeoBundle 'tomtom/tcomment_vim'
NeoBundle 'scrooloose/nerdtree'
" English
NeoBundle 'ujihisa/neco-look'
NeoBundle 'kana/vim-smartchr'
""NeoBundle 'https://bitbucket.org/kovisoft/slimv'

call neobundle#end()
filetype plugin indent on     " required!
filetype indent on
syntax on
NeoBundleCheck

"------------------------------------
" Multiplesearch
"------------------------------------
nnoremap  <ESC><ESC> :<C-u>nohlsearch<cr><Esc>
inoremap <silent> <ESC> <ESC>:set iminsert=0<CR>

" $BI=<($9$k:]$N?'?t$N@_Dj(B
let g:MultipleSearchMaxColors = 7

"------------------------------------
" syntastic
"------------------------------------
" $BJ]B8;~9=J8%A%'%C%/$NBP>]30$H$9$k%5%U%#%C%/%9$N;XDj(B
let g:syntastic_mode_map = { 'mode': 'active',
  \ 'passive_filetypes': ['tex'] }

"------------------------------------
" color
"------------------------------------
" colorscheme molokai
" colorscheme mustang 
colorscheme landscape
" colorscheme jellybeans
" colorscheme solarized 
"if g:colors_name ==? 'wombat'
"	  hi Visual gui=none guifg=khaki guibg=olivedrab
"endif
set background=dark

"------------------------------------
" YankRing
"------------------------------------
" , y $B$G%d%s%/MzNr(B
" YankRing.vim
" http://nanasi.jp/articles/vim/yankring_vim.html
" https://github.com/yuroyoro/dotfiles/blob/master/.vimrc.plugins_setting
" nmap ,y :YRShow<CR>

"------------------------------------
" yankround
"------------------------------------
nmap p <Plug>(yankround-p)
xmap p <Plug>(yankround-p)
nmap P <Plug>(yankround-P)
nmap gp <Plug>(yankround-gp)
xmap gp <Plug>(yankround-gp)
nmap gP <Plug>(yankround-gP)
nmap <C-p> <Plug>(yankround-prev)
nmap <C-n> <Plug>(yankround-next)
let g:yankround_max_history = 75
" nnoremap <silent>g<C-p> :<C-u>CtrlPYankRound<CR>

" $B%/%j%C%W%\!<%I6&M-(B
" http://vim-users.jp/2010/02/hack126/
set clipboard+=unnamedplus,unnamed

"------------------------------------
" indent-guides
"------------------------------------
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" vim$BN)$A>e$2$?$H$-$K!"<+F0E*$K(Bvim-indent-guides$B$r%*%s$K$9$k(B
let g:indent_guides_enable_on_vim_startup=1
" $B%,%$%I$r%9%?!<%H$9$k%$%s%G%s%H$NNL(B
let g:indent_guides_start_level=2
" $B<+F0%+%i!<$rL58z$K$9$k(B
let g:indent_guides_auto_colors=0
" $B4q?t%$%s%G%s%H$N%+%i!<(B
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#262626 ctermbg=lightgray
" $B6v?t%$%s%G%s%H$N%+%i!<(B
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#3c3c3c ctermbg=lightgray
" $B%O%$%i%$%H?'$NJQ2=$NI}(B
let g:indent_guides_color_change_percent = 30
" $B%,%$%I$NI}(B
let g:indent_guides_guide_size = 1

"------------------------------------
" Disable AutoComplPop.
"------------------------------------
let g:acp_enableAtStartup = 0
" Use neocomplcache.
let g:neocomplcache_enable_at_startup = 1
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'

" Define dictionary.
let g:neocomplcache_dictionary_filetype_lists = {
	\ 'default' : ''
	\ }

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplcache#undo_completion()
inoremap <expr><C-l>     neocomplcache#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
    return neocomplcache#smart_close_popup() . "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
inoremap <expr><C-y>  neocomplcache#close_popup()
inoremap <expr><C-e>  neocomplcache#cancel_popup()


"------------------------------------
" snippets
"------------------------------------
" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1

" set snippets directory
let g:neosnippet#snippets_directory='~/.vim/bundle/Neosnippet-snippets/neosnippets'

" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable() <Bar><bar> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable() <Bar><bar> neosnippet#jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif

" if has('python')
"     NeoBundleLazy 'Rip-Rip/clang_complete', {
"     \   'autoload': { 'filetypes': [ 'c', 'cpp', 'objc', 'objcpp' ] },
"     \ }
" endif
" NeoBundleLazy 'Shougo/neocomplcache-rsense', {
"    \   'autoload': { 'filetypes': [ 'ruby' ] },
"    \ }
"
" NeoBundleLazy 'mattn/zencoding-vim', {
" \   'autoload': { 'filetypes': [ 'html', 'haml', 'css' ] },
" \ }

"------------------------------------
" Vim-LaTeX
"------------------------------------
filetype plugin on
filetype indent on
set shellslash
set grepprg=grep\ -nH\ $*
let g:tex_flavor='latex'
let g:Imap_UsePlaceHolders = 1
let g:Imap_DeleteEmptyPlaceHolders = 1
let g:Imap_StickyPlaceHolders = 0
let g:Tex_DefaultTargetFormat = 'pdf'
"let g:Tex_FormatDependency_pdf = 'pdf'
let g:Tex_FormatDependency_pdf = 'dvi,pdf'
"let g:Tex_FormatDependency_pdf = 'dvi,ps,pdf'
let g:Tex_FormatDependency_ps = 'dvi,ps'
"let g:Tex_CompileRule_pdf = 'ptex2pdf -l -ot "-synctex=1 -interaction=nonstopmode -file-line-error-style" $*'
"let g:Tex_CompileRule_pdf = 'ptex2pdf -l -u -ot "-synctex=1 -interaction=nonstopmode -file-line-error-style" $*'
"let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
"let g:Tex_CompileRule_pdf = 'lualatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
"let g:Tex_CompileRule_pdf = 'luajitlatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
"let g:Tex_CompileRule_pdf = 'xelatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
let g:Tex_CompileRule_pdf = 'dvipdfmx $*.dvi'
"let g:Tex_CompileRule_pdf = 'ps2pdf $*.ps'
let g:Tex_CompileRule_ps = 'dvips -Ppdf -o $*.ps $*.dvi'
let g:Tex_CompileRule_dvi = 'platex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
"let g:Tex_CompileRule_dvi = 'uplatex -synctex=1 -interaction=nonstopmode -file-line-error-style $*'
let g:Tex_BibtexFlavor = 'pbibtex'
"let g:Tex_BibtexFlavor = 'upbibtex'
"let g:Tex_BibtexFlavor = 'bibtex'
"let g:Tex_BibtexFlavor = 'bibtexu'
let g:Tex_MakeIndexFlavor = 'mendex $*.idx'
"let g:Tex_MakeIndexFlavor = 'makeindex $*.idx'
"let g:Tex_MakeIndexFlavor = 'texindy $*.idx'
let g:Tex_UseEditorSettingInDVIViewer = 1
"let g:Tex_ViewRule_pdf = 'evince'
"let g:Tex_ViewRule_pdf = 'okular --unique'
"let g:Tex_ViewRule_pdf = 'zathura -s -x "vim --servername synctex -n --remote-silent +\%{line} \%{input}"'
"let g:Tex_ViewRule_pdf = 'qpdfview --unique'
"let g:Tex_ViewRule_pdf = 'pdfviewer'
"let g:Tex_ViewRule_pdf = 'texworks'
"let g:Tex_ViewRule_pdf = 'mupdf'
"let g:Tex_ViewRule_pdf = 'firefox -new-window'
"let g:Tex_ViewRule_pdf = 'chromium --new-window'
let g:Tex_ViewRule_pdf = 'acroread'
"let g:Tex_ViewRule_pdf = 'pdfopen -viewer ar9-tab'
let g:Tex_ViewRule_ps = 'evince'
"let g:Tex_ViewRule_ps = 'okular --unique'
"let g:Tex_ViewRule_ps = 'zathura'
"let g:Tex_ViewRule_ps = 'qpdfview --unique'
"let g:Tex_ViewRule_ps = 'gv --watch'
let g:Tex_ViewRule_dvi = 'pxdvi -watchfile 1'
"let g:Tex_ViewRule_dvi = 'xdvi -watchfile 1'
"------------------------------------
" quickrun
"------------------------------------
" set auto TeX make
let g:quickrun_config = {}
let g:quickrun_config['tex'] = {
      \   'command' : 'latexmk',
      \   'outputter' : 'error',
      \   'outputter/error/error' : 'quickfix',
      \   'cmdopt': '-pdfdvi',
      \   'exec': ['%c %o %s']
      \ }

" <C-c> $B$G<B9T$r6/@)=*N;$5$;$k(B
" quickrun.vim $B$,<B9T$7$F$$$J$$>l9g$K$O(B <C-c> $B$r8F$S=P$9(B
nnoremap <expr><silent> <C-c> quickrun#is_running() ?  quickrun#sweep_sessions() : "\<C-c>"

"------------------------------------
"setting for spell
"------------------------------------
map ^T :w!<CR>:!aspell check %<CR>:e! %<CR>
set spelllang=en,cjk

fun! s:SpellConf()
  redir! => syntax
  silent syntax
  redir END

  " set spell

  if syntax =~? '/<comment\>'
    syntax spell default
    syntax match SpellMaybeCode /\<\h\l*[_A-Z]\h\{-}\>/ contains=@NoSpell transparent containedin=Comment contained
  else
    syntax spell toplevel
    syntax match SpellMaybeCode /\<\h\l*[_A-Z]\h\{-}\>/ contains=@NoSpell transparent
  endif

  syntax cluster Spell add=SpellNotAscii,SpellMaybeCode
endfunc

augroup spell_check
  autocmd!
  autocmd BufReadPost,BufNewFile,Syntax * call s:SpellConf()
augroup END

"------------------------------------
" AutoDelLastSpace
"------------------------------------
function! RTrim()
  let s:cursor = getpos(".")
  %s/\s\+$//e
  call setpos(".", s:cursor)
endfunction

autocmd BufWritePre *.php,*.rb,*.js,*.bat call RTrim()

"------------------------------------
"vim-lightline
"------------------------------------
set laststatus=2

let s:lightline = expand('~/.vim/lightline.vim')
if isdirectory(s:lightline)
  set rtp+=~/.vim/lightline.vim

  let g:lightline = {
        \ 'colorscheme': 'landscape',
        \ 'component_function': {
        \   'filename': 'MyFilename',
        \ }
        \ }
else
  call system("git clone https://github.com/itchyny/lightline.vim ~/.vim/lightline.vim")
  set rtp+=~/.vim/lightline.vim

  let g:lightline = {
        \ 'colorscheme': 'landscape',
        \ 'component_function': {
        \   'filename': 'MyFilename',
        \ }
        \ }
endif

function! MyModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! MyReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &ro ? expand('%r') : ''
endfunction

function! MyFilename()
  return ('' != MyReadonly() ? MyReadonly() . ' ' : '') .
        \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
        \  &ft == 'unite' ? unite#get_status_string() :
        \  &ft == 'vimshell' ? substitute(b:vimshell.current_dir,expand('~'),'~','') :
        \ '' != expand('%f') ? expand('%f') : '[No Name]') .
        \ ('' != MyModified() ? ' ' . MyModified() : '')
endfunction      \ }

"------------------------------------
"vim-powerline
"------------------------------------
" set laststatus=2

" let g:Powerline_symbols='fancy'
" call Pl#Hi#Allocate({
"   \ 'black'          : 16,
"   \ 'white'          : 231,
"   \
"   \ 'darkestgreen'   : 22,
"   \ 'darkgreen'      : 28,
"   \
"   \ 'darkestcyan'    : 21,
"   \ 'mediumcyan'     : 117,
"   \
"   \ 'darkestblue'    : 24,
"   \ 'darkblue'       : 31,
"   \
"   \ 'darkestred'     : 52,
"   \ 'darkred'        : 88,
"   \ 'mediumred'      : 124,
"   \ 'brightred'      : 160,
"   \ 'brightestred'   : 196,
"   \
"   \ 'darkestyellow'  : 59,
"   \ 'darkyellow'     : 100,
"   \ 'darkestpurple'  : 57,
"   \ 'mediumpurple'   : 98,
"   \ 'brightpurple'   : 189,
"   \
"   \ 'brightorange'   : 208,
"   \ 'brightestorange': 214,
"   \
"   \ 'gray0'          : 233,
"   \ 'gray1'          : 235,
"   \ 'gray2'          : 236,
"   \ 'gray3'          : 239,
"   \ 'gray4'          : 240,
"   \ 'gray5'          : 241,
"   \ 'gray6'          : 244,
"   \ 'gray7'          : 245,
"   \ 'gray8'          : 247,
"   \ 'gray9'          : 250,
"   \ 'gray10'         : 252,
"   \ })
" " 'n': normal mode
" " 'i': insert mode
" " 'v': visual mode
" " 'r': replace mode
" " 'N': not active
" let g:Powerline#Colorschemes#my#colorscheme = Pl#Colorscheme#Init([
"   \ Pl#Hi#Segments(['SPLIT'], {
"     \ 'n': ['white', 'gray2'],
"     \ 'N': ['gray0', 'gray0'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['mode_indicator'], {
"     \ 'i': ['darkestgreen', 'white', ['bold']],
"     \ 'n': ['darkestcyan', 'white', ['bold']],
"     \ 'v': ['darkestpurple', 'white', ['bold']],
"     \ 'r': ['mediumred', 'white', ['bold']],
"     \ 's': ['white', 'gray5', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['fileinfo', 'filename'], {
"     \ 'i': ['white', 'darkestgreen', ['bold']],
"     \ 'n': ['white', 'darkestcyan', ['bold']],
"     \ 'v': ['white', 'darkestpurple', ['bold']],
"     \ 'r': ['white', 'mediumred', ['bold']],
"     \ 'N': ['gray0', 'gray2', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['branch', 'scrollpercent', 'raw', 'filesize'], {
"     \ 'n': ['gray2', 'gray7'],
"     \ 'N': ['gray0', 'gray2'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['fileinfo.filepath', 'status'], {
"     \ 'n': ['gray10'],
"     \ 'N': ['gray5'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['static_str'], {
"     \ 'n': ['white', 'gray4'],
"     \ 'N': ['gray1', 'gray1'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['fileinfo.flags'], {
"     \ 'n': ['white'],
"     \ 'N': ['gray4'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['currenttag', 'fileformat', 'fileencoding', 'pwd', 'filetype', 'rvm:string', 'rvm:statusline', 'virtualenv:statusline', 'charcode', 'currhigroup'], {
"     \ 'n': ['gray9', 'gray4'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['lineinfo'], {
"     \ 'n': ['gray2', 'gray10'],
"     \ 'N': ['gray2', 'gray4'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['errors'], {
"     \ 'n': ['white', 'gray2'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['lineinfo.line.tot'], {
"     \ 'n': ['gray2'],
"     \ 'N': ['gray2'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['paste_indicator', 'ws_marker'], {
"     \ 'n': ['white', 'brightred', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['gundo:static_str.name', 'command_t:static_str.name'], {
"     \ 'n': ['white', 'mediumred', ['bold']],
"     \ 'N': ['brightred', 'darkestred', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['gundo:static_str.buffer', 'command_t:raw.line'], {
"     \ 'n': ['white', 'darkred'],
"     \ 'N': ['brightred', 'darkestred'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['gundo:SPLIT', 'command_t:SPLIT'], {
"     \ 'n': ['white', 'darkred'],
"     \ 'N': ['white', 'darkestred'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:focus', 'ctrlp:byfname'], {
"     \ 'n': ['brightpurple', 'darkestpurple'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:prev', 'ctrlp:next', 'ctrlp:pwd'], {
"     \ 'n': ['white', 'mediumpurple'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:item'], {
"     \ 'n': ['darkestpurple', 'white', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:marked'], {
"     \ 'n': ['brightestred', 'darkestpurple', ['bold']],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:count'], {
"     \ 'n': ['darkestpurple', 'white'],
"     \ }),
"   \
"   \ Pl#Hi#Segments(['ctrlp:SPLIT'], {
"     \ 'n': ['white', 'darkestpurple'],
"     \ }),
"   \ ])
" let g:Powerline_colorscheme='my'
" let g:Powerline_mode_n = 'NORMAL'

"------------------------------------
" set encoding
"------------------------------------
set encoding=utf-8
set fileencodings=iso-2022-jp,ucs-bom,iso-2022-jp-3,iso-2022-jp,euc-jp,sjis,eucjp-ms,euc-jisx0213,cp932,utf-8
