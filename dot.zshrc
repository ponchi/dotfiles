###
### MATSUO & TSUMURA lab. 
###   ~/.zshrc template
###  feel free to edit this file at your own risk
###
### Last Modified: 2015/12/09 22:03
### Created:       2007/04/03 17:50

# Keybind
bindkey -e # emacs風
# bindkey -v # vi風

#PS1="${USER}@${HOST%%.*}:%~/%(!.#.%%) "
 
# 補完で小文字でも大文字にマッチさせる
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
 
# ../ の後は今いるディレクトリを補完しない
zstyle ':completion:*' ignore-parents parent pwd ..
 
# sudo の後ろでコマンド名を補完する
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                   /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin
 
# ps コマンドのプロセス名補完
zstyle ':completion:*:processes' command 'ps x -o pid,s,args'

# 補完候補を移動+ハイライト
zstyle ':completion:*:default' menu select=1

# # セパレータを設定する
# zstyle ':completion:*' list-separator '-->'
# zstyle ':completion:*:manuals' separate-sections true
 
# コマンドラインスタック
show_buffer_stack() {
  POSTDISPLAY="
  stack: $LBUFFER"
  zle push-line-or-edit
}
zle -N show_buffer_stack
bindkey '^Q' show_buffer_stack
bindkey -v '^Q' show_buffer_stack


# history                                                                
HISTFILE=~/.zsh_history
HISTSIZE=300000
SAVEHIST=300000

#setopt prompt_subst
#%B 太字 %K 背景色 %F 文字色 %U 下線
#PROMPT=$'%{\e[$[41]m%}'"%n@%m"$'\e[m'":%~/
local p_mark="%B%(?,%F{green},%F{red})%(!,#,%%)%f%b"
#local p_mark="%B%(?,%{${fg[green]}%},%{${fg[red]}%})%(!,#,%%)%{$reset_color%}%b"
#local p_info="%n@%U%m%u"
#PROMPT="$p_info$p_mark "

local p_time="%F{cyan}%D %@%f"
#RPROMPT="%B%F{blue}[%~]%f%b"
# local rp_time="%{${fg[green]}%}%@%{$reset_color%}"
# RPROMPT="%B%{${fg[blue]}%}[%~]%{$reset_color%}%b"

PROMPT2="(% ) %(!,#,%%) "
SPROMPT="correct: %R -> %r ?  [n,y,a,e]: "

#PROMPT 2culm
local p_info="%n@%U%m%u"
local p_cdir="%B%F{blue}[%~]%f%b"$'\n'

PROMPT=" $p_info $p_time $p_cdir$p_mark "

# display vim mode
# function zle-line-init zle-keymap-select {
# case $KEYMAP in
#   vicmd)
#     # RPROMPT="⮂%{$fg_bold[blue]$bg[white]%} NORMAL %{$reset_color%}⮀ "
#     RPROMPT="%{$fg_bold[blue]$bg[white]%} NORMAL %{$reset_color%}"
#     ;;
#   main|viins)
#     # RPROMPT="⮂%{$fg_bold[green]$bg[white]%} INSERT %{$reset_color%}⮀ "
#     RPROMPT="%{$fg_bold[green]$bg[white]%} INSERT %{$reset_color%}"
#     ;;
# esac
#   zle reset-prompt
# }
# zle -N zle-line-init
# zle -N zle-keymap-select

# vcs_info
case ${OSTYPE} in
  darwin*)
    #for Mac OS X
    autoload -Uz vcs_info
    zstyle ':vcs_info:*' formats '(%s)-[%b]'
    zstyle ':vcs_info:*' actionformats '(%s)-[%b|%a]'
    precmd () {
      psvar=()
      LANG=en_US.UTF-8 vcs_info
      [[ -n "$vcs_info_msg_0_" ]] && psvar[1]="$vcs_info_msg_0_"
    }
    autoload colors
    ;;
esac
#RPROMPT="%1(v|%F{green}%1v%f|)"

#
# aliases
#
case ${OSTYPE} in
  darwin*)
    #for Mac OS X
    alias ls='ls -G -F'
    alias grep='grep --with-filename --line-number --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias emacsclient='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient' 
    alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
    alias vim='/Applications/MacVim.app/Contents/MacOS/vim' 
    alias gvim='vim -g' 

    alias sshe='cocot -t UTF-8 -p EUC-JP -- ssh'
    ;;

  linux*)
    #for Linux
    alias ls='ls -F --color=always'
    alias grep='grep --with-filename --line-number --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    # use original compiled emacs and vim
    case "`hostname`" in 
      miura*|fraise*|tiramisu*)
        alias vim='/usr/local/bin/vim'
        alias emacs='/usr/local/bin/emacs'
        alias emacsclient='/usr/local/bin/emacsclient'
        ;;
    esac 

    alias gvim='vim -g'
    # alias j='autojump'
    alias ordesk='rdesktop -k ja -a 16 -x l -g 1270x980 133.68.15.16'
    alias ordesk+='rdesktop -k ja -a 16 -x l -g 1910x1015 133.68.15.16'
    alias rdesk='rdesktop -a 16 -x l -g 1270x990'
    alias rdesk+='rdesktop -a 16 -x l -g 1910x1015'
    alias mktex='bash ~/tex.sh'
    alias mkbtex='bash ~/btex.sh'
    alias jnethack='cocot -t UTF-8 -p EUC-JP -- /usr/local/games/jnethack'

    alias sshe='cocot -t UTF-8 -p EUC-JP -- ssh'
    # alias tmux="LD_LIBRARY_PATH=/usr/local/lib /usr/local/bin/tmux"
    ;;

  solaris*)
    #for Solaris
    alias ls='gls -F --color'
    ;;
esac

alias la='ls -A'
alias ll='ls -l'
alias lla='la -l'
alias lal='la -l'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'

alias chvim='bindkey -v'
alias chemacs='bindkey -e'

alias du='du -hk'
if type colordiff >/dev/null 2>&1; then
  alias diff='colordiff'
fi
alias sudo='sudo '

alias sozr='source ~/.zshrc'

#global aliases
# alias -g CSE='cin15159@cse.cs.nitech.ac.jp'
# alias -g MATLAB='r-yamada@matnfs.matlab.nitech.ac.jp'

alias -g G='| grep'
alias -g L='| less'
alias -g H='| head'
alias -g T='| tail'
alias -g S='| sort'
alias -g W='| wc'
alias -g X='| xargs'
if type pv >/dev/null 2>&1; then
  alias -g P='| pv'
fi
set -o noclobber		# no overwrite when redirect

# user file-creation mask
umask 022

#
# zsh gadgets
#
bindkey -e
bindkey ' ' magic-space

setopt always_last_prompt
setopt auto_cd
setopt auto_list
setopt auto_menu
setopt auto_param_slash
setopt auto_pushd
setopt auto_remove_slash
setopt auto_resume
setopt bad_pattern
setopt beep
setopt brace_ccl
setopt extended_glob
setopt pushd_silent
setopt complete_aliases
setopt complete_in_word
setopt correct_all
setopt csh_null_glob
setopt extended_glob
# unsetopt flow_control           # disable stop=^s and start=^q
setopt no_flow_control
setopt glob_dots
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_nodups
setopt interactive_comments
setopt list_ambiguous
setopt list_types
setopt no_beep
setopt path_dirs
setopt print_eight_bit
setopt pushd_ignore_dups
setopt pushd_to_home
setopt share_history
setopt short_loops
setopt sun_keyboard_hack
setopt rc_expand_param
unsetopt rmstar_wait

limit -h coredumpsize 0

# 補完候補をカラー表示する
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

## 出力の文字列末尾に改行コードが無い場合でも表示
unsetopt promptcr
## --prefix=/usr などの = 以降も補完
setopt magic_equal_subst

# 今入力している内容から始まるヒストリを探す
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
# emacs bind
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end
# vi bind
bindkey -M vicmd "k" history-beginning-search-backward-end
bindkey -M vicmd "j" history-beginning-search-forward-end
bindkey -M viins "^P" history-beginning-search-backward-end
bindkey -M viins "^N" history-beginning-search-forward-end


# コマンドハイライト
if [ -f ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

#=============================
# source auto-fu.zsh
#=============================
if [ -f ~/.zsh/auto-fu.zsh/auto-fu.zsh ]; then
    source ~/.zsh/auto-fu.zsh/auto-fu.zsh
    function zle-line-init () {
        auto-fu-init
    }
    zle -N zle-line-init
    zstyle ':completion:*' completer #「-azfu-」を表示させない
    zstyle ':auto-fu:var' postdisplay $''
fi

#=============================
# autojump for OS X
#=============================
# case $OSTYPE in
#   darwin*)
#     export FPATH="$FPATH:/opt/local/share/zsh/site-functions/"
#     if [ -f /opt/local/etc/profile.d/autojump.zsh ]; then
#       . /opt/local/etc/profile.d/autojump.zsh
#     fi
    # ;;
    #
    # linux*)
    # # export FPATH="$FPATH:/usr/local/share/zsh/site-functions/"
    # if [ -f /usr/local/etc/profile.d/autojump.zsh ]; then
    #     source /usr/local/etc/profile.d/autojump.zsh
    # fi
  # esac

# complement
autoload -Uz compinit
compinit

# set keybind
# bindkey -v

#
# PATH
#
unset SSH_AUTH_SOCK

# just type 'cd ...' to get 'cd ../..'
rationalise-dot() {
  if [[ $LBUFFER == *.. ]] ; then
    LBUFFER+=/..
  else
    LBUFFER+=.
  fi
}
zle -N rationalise-dot
bindkey . rationalise-dot

#
# for quota 
#
case $OSTYPE in
  linux*)
    #for Linux
    case "`hostname -d`" in
      matlab.nitech.ac.jp)
        if test -e /usr/sbin/quota; then
          echo -n '[31m'
          /usr/sbin/quota
          echo -n '[00m'
        fi
        ;;
    esac
    ;;

  linux*)
    #for and Solaris
    if test -e /usr/sbin/quota; then
      echo -n '[31m'
      /usr/sbin/quota
      echo -n '[00m'
    fi
    ;;

  darwin*)
    #for Mac OS X (can't use "-d" option)
    case "`hostname -f`" in
      *matlab.nitech.ac.jp)
        if test -e /usr/sbin/quota; then
          echo -n '[31m'
          /usr/sbin/quota
          echo -n '[00m'
        fi
        ;;
    esac
    ;;
esac

# source local rcfile
if [ -f ~/.zshrc_local ]; then
  source ~/.zshrc_local
fi
