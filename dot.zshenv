###
### r-yamada who is member of MATSUO & TSUMURA lab. 
###   ~/.zshenv file
###  feel free to edit this file at your own risk
###
### Last Modified: 2015/12/09 22:03
### Created:       2007/04/03 17:50

#
# environment variables
#
#export PS1="\u@\h:\w/\$ "
export PAGER="less"
export LESS="-imqMXR -x 8"
export LESSOPEN='| src-hilite-lesspipe.sh %s'
export EDITOR="vim"
# export LESSCOLOR=re

case ${OSTYPE} in
  darwin*)
    #for Mac OS X
    autoload -Uz colors
    colors
    export LANG=ja_JP.UTF-8
    export CLICOLOR=1
    export PATH=/opt/local/bin:/opt/local/sbin/:/usr/local/bin/node:/usr/local/bin:${PATH}
    export NODE_PATH=`npm root -g`
    ;;

  linux*)
    #for Linux
    autoload -Uz colors
    colors
    export LD_LIBRARY_PATH=/usr/local/lib
    export TERM=xterm-256color
    export INFOPATH=/usr/local/texlive/2013/texmf-dist/doc/info
    export TEXMFHOME=~/texmf	# dont set TEXINPUTS
    export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
    export CVSROOT=/project/camp/cvs  # for camp group
    export CVS_RSH=ssh

    export PATH=/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/usr/sbin:/sbin:/usr/local/texlive/2014/bin/i386-linux:$HOME/.nodebrew/current/bin:${PATH}
    export MAN=~/share/man:/usr/local/texlive/2013/texmf-dist/doc/man:${MAN}
    export INFOPATH=/usr/local/texlive/2014/texmf-dist/doc/info:${INFOPATH}
    export MANPATH=//usr/local/texlive/2014/texmf-dist/doc/man:${MANPATH}
    export TMPDIR=/tmp
    export NODE_PATH=`npm root -g`
    ;;

  solaris*)
    #for Solaris
    export LANG='ja'
    export INFOPATH=/usr/local/texlive/2013/texmf-dist/doc/info
    export TEXMFHOME=~/texmf	# dont set TEXINPUTS
    export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
    export CVSROOT=/project/camp/cvs  # for camp group
    export CVS_RSH=ssh

    export MANPATH=/usr/man:/opt/local/man:/usr/sfw/man:/opt/csw/man:/opt/SUNWspro/man:/usr/openwin/man:/usr/dt/man:/usr/X11/man:/usr/local/texlive/2013/texmf-dist/doc/man
    export PATH=/opt/local/bin:/usr/bin:/usr/sfw/bin:/opt/csw/bin:/usr/ccs/bin:/opt/SUNWspro/bin:/usr/openwin/bin:/usr/dt/bin:/usr/X11/bin:/usr/local/texlive/2013/bin:${PATH}
    # stty erase ^h
    # stty intr  ^c
    # stty susp  ^z
    ;;
esac

# Read .zshrc
# * set include guard
# if test -z "$_HOMEZSHRC"; then
# # if [ -z "$_HOMEZSHRC" ]; then
#   readonly _HOMEZSHRC=true
#   test -r ~/.zshrc && . ~/.zshrc
# fi
