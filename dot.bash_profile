###
### MATSUO & TSUMURA lab. 
###   ~/.bash_profile template
###  feel free to edit this file at your own risk
###
### Last Modified: 2015/12/09 16:36
### Created:       2007/04/03 17:50

#
# environment variables
#
#export PS1="\u@\h:\w/\$ "
export PS1=" \u@\h:\[\e[34;1m\]\w/\[\e[0m\]\n\[\e[32;1m\]\$\[\e[0m\] "
export PAGER="less"
export LESS="-imqMXR -x 8"
# export LESSCOLOR=red
export EDITOR="vi"
#
case ${OSTYPE} in
  solaris*)
    #for Mac OS X
    export TEXMFHOME=~/texmf	# dont set TEXINPUTS
    export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
    export CVSROOT=/project/camp/cvs  # for camp group
    ;;

  linux*)
    #for Linux
    export TERM=xterm-256color
    export TEXMFHOME=~/texmf	# dont set TEXINPUTS
    export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
    export CVSROOT=/project/camp/cvs  # for camp group
    ;;
esac

export CVS_RSH=ssh
#export CVSROOT=/home/r-yamada/cvsrep
export CVSEDITOR="emacs"

#exec /bin/zsh

# history                                                                
HISTFILE=~/.bash_history
HISTSIZE=300000
HISTFILESIZE=300000
export HISTCONTROL=ignoreboth

#
# PATH
#
case $OSTYPE in
  linux*)
    #for Linux
    export PATH=/bin:/usr/bin:/usr/X11/bin:/usr/local/bin:/sbin:~/bin:~/share/bin:${PATH}
    export MAN=~/share/man:${MAN}
    ;;

  darwin*)
    #for Mac OS X
    export PATH=~/bin:/usr/games/bin:/opt/local/bin:/opt/local/sbin/:$PATH
    export CLICOLOR=1
    ;;

  solaris*)
    #for Solaris
    export LANG='ja'
    export MANPATH=/usr/man:/opt/local/man:/usr/sfw/man:/opt/csw/man:/opt/SUNWspro/man:/usr/openwin/man:/usr/dt/man:/usr/X11/man
    export PATH=/opt/local/bin:/usr/bin:/usr/sfw/bin:/opt/csw/bin:/usr/ccs/bin:/opt/SUNWspro/bin:/usr/openwin/bin:/usr/dt/bin:/usr/X11/bin:~/bin
    ;;
esac

# read test
# echo "profile"

# Read .bashrc
# * set include guard
if test -z "$_HOMEBASHRC"; then
# if [ -z "$_HOMEBASHRC" ]; then
  readonly _HOMEBASHRC=true
  test -r ~/.bashrc && . ~/.bashrc
fi
