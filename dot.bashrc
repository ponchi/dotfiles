###
### MATSUO & TSUMURA lab. 
###   ~/.bashrc template
###  feel free to edit this file at your own risk
###
### Last Modified: 2015/12/09 16:36
### Created:       2007/04/03 17:50

#share history
# function share_history {
#    history -a
#    history -c
#    history -r
# }
# PROMPT_COMMAND=' share_history'
# shopt -u histappend

#
# aliases
#
case ${OSTYPE} in
  darwin*)
    #for Mac OS X
    alias emacsclient='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient' 
    alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
    alias vim='/Applications/MacVim.app/Contents/MacOS/vim' 
    alias gvim='vim -g' 

    alias ls='ls -G -F'
    ;;

  linux*)
    #for Linux

    # use original compiled vim
    case "`hostname`" in
      miura*|tiramisu*|fraise*)
        alias vim='/usr/local/bin/vim'
        ;;
    esac

    alias gvim='vim -g'
    # alias ordesk='rdesktop -k ja -a 16 -x l -g 1890x990 133.68.15.16'
    alias ordesk='rdesktop -k ja -a 16 -x l -g 1270x990 133.68.15.16'
    alias ordesk+='rdesktop -k ja -a 16 -x l -g 1910x1015 133.68.15.16'
    alias rdesk='rdesktop -a 16 -x l -g 1280x960'
    alias mktex='bash ~/tex.sh'
    alias mkbtex='bash ~/btex.sh'
    alias ls='ls -F --color=always'
    # alias rogue='~/rogue/rogue54'
    # alias omega='cd ~/OmegaRPG-master/build/unix/ ; ./omega'
    # alias jnethack='cocot -t UTF-8 -p EUC-JP -- /usr/local/bin/jnethack'
    # alias tmux="LD_LIBRARY_PATH=/usr/local/lib /usr/local/bin/tmux"
    ;;

  solaris*)
    #for Solaris
    # stty erase ^h
    # stty intr  ^c
    # stty susp  ^z
    alias ls='gls -F --color'
    ;;
esac

alias sshe='cocot -t UTF-8 -p EUC-JP -- ssh'
alias du='du -hk'
if type colordiff >/dev/null 2>&1; then
  alias diff='colordiff'
fi

alias la='ls -A'
alias ll='ls -l'
alias lla='la -l'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'

set -o noclobber		# no overwrite when redirect

# user file-creation mask
umask 022
ulimit -c 0

# set keybind
## like vi
# set -o vi
## like emacs
set -o emacs

# for quota
case $OSTYPE in
  linux*|darwin*)
    case "`hostname -f`" in
      *.matlab.nitech.ac.jp)
        if test -e /usr/sbin/quota; then
          echo -n '[31m'
          /usr/sbin/quota
          echo -n '[00m'
        fi
        ;;
    esac
    ;;

  solaris*)
    if test -e /usr/sbin/quota; then
      echo -n '[31m'
      /usr/sbin/quota
      echo -n '[00m'
    fi
    ;;
esac

# source local rcfile
if [ -f ~/.bashrc_local ]; then
  source ~/.bashrc_local
fi

# read test
# echo "rc"

case "`hostname`" in 
  miura*|tiramisu*|framboise*)
    /bin/zsh
    ;;

  fraise*)
    /usr/local/bin/zsh
    ;;
esac
