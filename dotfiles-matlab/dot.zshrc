### MATSUO & TSUMURA lab. 
###   ~/.bash_profile template
###  feel free to edit this file at your own risk
###
### Last Modified: 2008/01/07 16:30
### Created:       2007/04/03 17:50

#
# environment variables
#
#export PS1="\u@\h:\w/\$ "
export PAGER="less"
export LESS="-imqMXR -x 8"
export EDITOR="vim"
# export LESSCOLOR=re

# Keybind
bindkey -e # emacs風
# bindkey -v # vi風

case ${OSTYPE} in
    darwin*)
        #for Mac OS X
	autoload -Uz colors
	colors
	export LANG=ja_JP.UTF-8
        ;;

    linux*)
	export LANG=ja_JP.UTF-8
        #for Linux
	autoload -Uz colors
	colors
	export INFOPATH=/usr/local/texlive/2013/texmf-dist/doc/info
	export TEXMFHOME=~/texmf	# dont set TEXINPUTS
	export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
	export CVSROOT=/project/camp/cvs  # for camp group
	export CVS_RSH=ssh
        ;;

    solaris*)
	#for Solaris
	export LANG='ja'
	export INFOPATH=/usr/local/texlive/2013/texmf-dist/doc/info
	export TEXMFHOME=~/texmf	# dont set TEXINPUTS
	export BIBCITEDIR=${TEXMFHOME}/bibtex/bib
	export CVSROOT=/project/camp/cvs  # for camp group
	export CVS_RSH=ssh
	;;
esac

# Keybind
bindkey -e

#PS1="${USER}@${HOST%%.*}:%~/%(!.#.%%) "
 
# 補完で小文字でも大文字にマッチさせる
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
 
# ../ の後は今いるディレクトリを補完しない
zstyle ':completion:*' ignore-parents parent pwd ..
 
# sudo の後ろでコマンド名を補完する
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
   /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin
 
# ps コマンドのプロセス名補完
zstyle ':completion:*:processes' command 'ps x -o pid,s,args'
 
# 補完候補を移動+ハイライト
zstyle ':completion:*:default' menu select=1
 
# コマンドラインスタック
show_buffer_stack() {
  POSTDISPLAY="
stack: $LBUFFER"
  zle push-line-or-edit
}
zle -N show_buffer_stack
bindkey '^Q' show_buffer_stack
bindkey -v '^Q' show_buffer_stack

# just type 'cd ...' to get 'cd ../..'
rationalise-dot() {
  if [[ $LBUFFER == *.. ]] ; then
    LBUFFER+=/..
  else
    LBUFFER+=.
  fi
}
zle -N rationalise-dot
bindkey . rationalise-dot

# history                                                                
HISTFILE=~/.zsh_history
HISTSIZE=300000
SAVEHIST=300000

#setopt prompt_subst
#%B 太字 %K 背景色 %F 文字色 %U 下線
#PROMPT=$'%{\e[$[41]m%}'"%n@%m"$'\e[m'":%~/
#local p_mark="%B%(?,%F{green},%F{red})%(!,#,%%)%f%b"
local p_mark="%B%(?,%{${fg[green]}%},%{${fg[red]}%})%(!,#,%%)%{$reset_color%}%b"
#local p_info="%n@%U%m%u"
#PROMPT="$p_info$p_mark "

#local rp_time="%F{green}%@%f"
#RPROMPT="%B%F{blue}[%~]%f%b"
local rp_time="%{${fg[cyan]}%}%D %@%{$reset_color%}"
#RPROMPT="%B%{${fg[blue]}%}[%~]%{$reset_color%}%b"

PROMPT2="(% ) %(!,#,%%) "
SPROMPT="correct: %R -> %r ?  [n,y,a,e]: "

#PROMPT 2culm
local p_info="%n@%U%m%u"
local p_cdir="%B%{${fg[blue]}%}[%~]%{$reset_color%}%b"$'\n'
#PROMPT=" $p_info $rp_time $p_cdir$p_mark "
PROMPT=" $p_info:$p_cdir$p_mark "
#RPROMPT="$p_info" 

# vcs_info
case ${OSTYPE} in
    darwin*)
        #for Mac OS X
	autoload -Uz vcs_info
	zstyle ':vcs_info:*' formats '(%s)-[%b]'
	zstyle ':vcs_info:*' actionformats '(%s)-[%b|%a]'
	precmd () {
	    psvar=()
	    LANG=en_US.UTF-8 vcs_info
	    [[ -n "$vcs_info_msg_0_" ]] && psvar[1]="$vcs_info_msg_0_"
	}
	autoload colors
        ;;
esac
#RPROMPT="%1(v|%F{green}%1v%f|)"

#
# aliases
#
case ${OSTYPE} in
    darwin*)
        #for Mac OS X
    alias ls='ls -G -F'
	alias emacsclient='/Applications/Emacs.app/Contents/MacOS/bin/emacsclient' 
	alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
	alias vim='/Applications/MacVim.app/Contents/MacOS/vim' 
	alias gvim='/Applications/MacVim.app/Contents/MacOS/MacVim' 

	alias sshe='cocot -t UTF-8 -p EUC-JP -- ssh'
        ;;

    linux*)
        #for Linux
	alias ls='ls -F --color=always'
    alias tailf='tail -f'
	alias vim='/usr/local/bin/vim'
	alias gvim='/usr/local/bin/gvim'
	alias ordesk='rdesktop -k ja -a 16 -x l -g 1890x990 133.68.15.16'
	alias rdesk='rdesktop -a 16 -x l -g 1280x960'
	alias mktex='bash ~/tex.sh'
	alias mkbtex='bash ~/btex.sh'
	#alias rogue='~/rogue/rogue54'
	alias omega='cd ~/OmegaRPG-master/build/unix/ ; ./omega'
	alias jnethack='cocot -t UTF-8 -p EUC-JP -- /usr/local/bin/jnethack'

	alias sshe='cocot -t UTF-8 -p EUC-JP -- ssh'
	#alias tmux="LD_LIBRARY_PATH=/usr/local/lib /usr/local/bin/tmux"
        ;;

    solaris*)
	#for Solaris
	alias ls='gls -F --color'
	;;
esac

alias emacsn='emacs -nw'
alias la='ls -A'
alias ll='ls -l'
alias lla='la -l'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'

alias du='du -hk'
alias chvim='bindkey -v'
alias chemacs='bindkey -e'
# alias diff='colordiff'
alias sudo='sudo '

#global aliases
alias -g CSE='cin15159@cse.cs.nitech.ac.jp'
alias -g MATLAB='r-yamada@matnfs.matlab.nitech.ac.jp'

alias -g G='| grep'
alias -g L='| less'
alias -g H='| head'
alias -g T='| tail'
alias -g S='| sort'
alias -g W='| wc'
alias -g X='| xargs'
set -o noclobber		# no overwrite when redirect

# user file-creation mask
umask 022

#
# zsh gadgets
#
bindkey -e
bindkey ' ' magic-space

setopt always_last_prompt
setopt auto_cd
setopt auto_list
setopt auto_menu
setopt auto_param_slash
setopt auto_pushd
setopt auto_remove_slash
setopt auto_resume
setopt bad_pattern
setopt beep
setopt brace_ccl
setopt extended_glob
setopt pushd_silent
setopt complete_aliases
setopt complete_in_word
setopt correct_all
setopt csh_null_glob
setopt extended_glob
unsetopt flow_control           # disable stop=^s and start=^q
setopt glob_dots
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_nodups
setopt interactive_comments
setopt list_ambiguous
setopt list_types
setopt no_beep
setopt path_dirs
setopt print_eight_bit
setopt pushd_ignore_dups
setopt pushd_to_home
setopt share_history
setopt short_loops
setopt sun_keyboard_hack
setopt rc_expand_param

limit -h coredumpsize 0

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

## 出力の文字列末尾に改行コードが無い場合でも表示
unsetopt promptcr
## --prefix=/usr などの = 以降も補完
setopt magic_equal_subst

# 履歴検索機能のショートカット
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
# emacs bind
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end
# vi bind
bindkey -M vicmd "k" history-beginning-search-backward-end
bindkey -M vicmd "j" history-beginning-search-forward-end
bindkey -M viins "^P" history-beginning-search-backward-end
bindkey -M viins "^N" history-beginning-search-forward-end

# コマンドハイライト
if [ -f ~r-yamada/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source ~r-yamada/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# complement
autoload -Uz compinit
compinit

#
# PATH
#
unset SSH_AUTH_SOCK
unset SSH_ASKPASS

case $OSTYPE in
    linux*)
        #for Linux
	export PATH=/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/sbin:~/bin:~/share/bin:/usr/local/texlive/2013/bin/i386-linux:${PATH}
	export MAN=~/share/man:/usr/local/texlive/2013/texmf-dist/doc/man:${MAN}
	echo -n '[31m'
	/usr/bin/quota -q
	echo -n '[00m'
        ;;

    solaris*)
        #for Solaris
	export MANPATH=/usr/man:/opt/local/man:/usr/sfw/man:/opt/csw/man:/opt/SUNWspro/man:/usr/openwin/man:/usr/dt/man:/usr/X11/man:/usr/local/texlive/2013/texmf-dist/doc/man
	export PATH=/opt/local/bin:/usr/bin:/usr/sfw/bin:/opt/csw/bin:/usr/ccs/bin:/opt/SUNWspro/bin:/usr/openwin/bin:/usr/dt/bin:/usr/X11/bin:~/bin:/usr/local/texlive/2013/bin:${PATH}
	stty erase ^h
	stty intr  ^c
	stty susp  ^z
	echo -n '[31m'
	/usr/sbin/quota
	echo -n '[00m'
        ;;

    darwin*)
        #for Mac OS X
    export CLICOLOR=1
	export PATH=~/bin:/opt/local/bin:/opt/local/sbin/:${PATH}
        ;;
esac

# source local rcfile
# if [ -f ~/.zshrc_local ]; then
#     source ~/.zshrc_local
# fi

# vim:filetype=zsh
