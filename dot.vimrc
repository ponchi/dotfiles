"------------------------------------
" Load settings (for Linux)
"------------------------------------
" ~/Dropbox/dotfiles/dot.vimrc.defaultが存在する場合のみ設定を読み込む
let s:OSTYPE = system('uname')
if s:OSTYPE == "Linux\n" || s:OSTYPE == "SunOS\n"
  let s:default_vimrc = expand('~/Dropbox/dotfiles/dot.vimrc.default')
  if filereadable(s:default_vimrc)
    execute 'source ' . s:default_vimrc
  endif
  let s:linux_vimrc = expand('~/Dropbox/dotfiles/dot.vimrc.linux')
  if filereadable(s:linux_vimrc)
    execute 'source ' . s:linux_vimrc
  endif

"------------------------------------
" Load settings (for OS X)
"------------------------------------
elseif s:OSTYPE == "Darwin\n"
  let s:mac_vimrc = expand('~/Dropbox/dotfiles/dot.vimrc.mac')
  if filereadable(s:mac_vimrc)
    execute 'source ' . s:mac_vimrc
  endif
endif

