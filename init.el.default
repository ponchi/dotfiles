;-*- emacs-lisp -*- 
;;;
;;; MATSUO & TSUMURA lab.
;;;   ~/.emacs template
;;;  feel free to edit this file at your own risk
;;;
;;; Last Modified: 2012/04/25
;;; Created:       2007/04/03

;;; switch variables
(defvar matlab:use-wl		t) ;; set this t to use Wanderlust
(defvar matlab:use-pline		t) ;; set this t to use powerline

;;
;;load path
;;
(setq load-path (append
		 (list "~/.emacs.d/site-lisp"
		       "~/.emacs.d/site-lisp/twittering-mode"
		       "~/.emacs.d/site-lisp/rail"
		       "~/.emacs.d/site-lisp/semi"
		       "~/.emacs.d/site-lisp/wl"
		       "~/.emacs.d/site-lisp/apel"
		       "~/.emacs.d/site-lisp/flim"
		       "~/.emacs.d/site-lisp/yatex"
		       "~/.emacs.d/site-lisp/emu")
		 load-path))

;;;
;;; Key customize
;;;
(global-set-key [(control h)]	'delete-backward-char)
(global-set-key [(control \\)]	'toggle-input-method) ; C-\ for Japanese input
(global-set-key [(meta g)]	'goto-line)

;;;
;;; バックアップファイルの置き場所を指定する
;;;
(setq make-backup-files t)
(setq backup-directory-alist
      (cons (cons "\\.*$" (expand-file-name "~/.emacs.d/bak"))
            backup-directory-alist))

;;;
;;; Language
;;;
(case window-system ((ns mac) (setq default-input-method "MacOSX")))
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)
(setq default-file-name-coding-system 'utf-8)

;;;
;;; highlight
;;;
(require 'font-lock)
;(turn-on-font-lock-if-enabled)
(global-font-lock-mode t)
(transient-mark-mode t)	; mark
(require 'paren)
(show-paren-mode t)	; paren

;;;
;;; emacsclient
;;;
(server-start)

;;;
;;; tramp
;;;
(if (locate-library "tramp")
    (require 'tramp))
(setq-default tramp-persistency-file-name nil)

;;;
;;; yatex
;;;
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)

;;;
;;;autoinsert
;;;
(require 'autoinsert)

;;;
;;; Mouse Wheel
;;;
(mwheel-install)
(setq mouse-wheel-follow-mouse t)

;; Wanderlust
;;
(when matlab:use-wl
  (autoload 'wl		"wl" "wanderlust" t)
  (setq smtp-local-domain	"matlab.nitech.ac.jp"
	wl-smtp-posting-server	"mail.matlab.nitech.ac.jp" ;; smtp
	elmo-imap4-default-server	"mail.matlab.nitech.ac.jp" ;; imap
	elmo-imap4-default-user "r-yamada"
	wl-biff-check-folder-list	'("%INBOX")
;;	wl-from			(concat (user-login-name) "@matlab.nitech.ac.jp")
	wl-message-id-domain	"matlab.nitech.ac.jp"
;;	wl-use-ldap		t
;;	wl-ldap-server		"mats.matlab.nitech.ac.jp"
;;	wl-ldap-base		"dc=matlab,dc=nitech,dc=ac,dc=jp"
	wl-draft-reply-buffer-style 'keep
	wl-draft-folder		"..Draft"
;;	wl-trash-folder		"..Trash"
	wl-queue-folder		"..queue"
	wl-interactive-send	t)
  )
(when (string-match "solaris" system-configuration)
	(setq ldap-search-program-arguments '("-L" "-T")))

;;; SEMI (for wl etc)
(setq mail-self-blind		t
      mime-edit-split-message   nil
      mime-header-accept-quoted-encoded-words t
      mime-view-ignored-field-list
      '(".*Received:" ".*Path:" ".*Id:" "^References:"
        "^Replied:" "^Errors-To:" "^Lines:" ".*Host:" "^Xref:"
        "^Precedence:" "^Status:" "^X-VM-.*:"
        "^Mail-Reply-To:" "^Mail-Followup-To:" "^In-Reply-To:" "^X-*"
        "^Posted:" "^Mailing-List:" "^Delivered-To:" ;;"^Date:"
        "^Content-Transfer-Encoding:" "^Message-Id:"
        "^List-Help:" "^List-Post:" "^List-Subscribe:"
        "^List-Unsubscribe:" "^List-Archive:"
        )
      mime-view-visible-field-list
      '("^Dnas.*:" "^Message-Id:"
        "^User-Agent:" "^X-Mailer:" "^X-Newsreader:" "^X-MimeOLE:" "^X-Emacs:"
        "^Content-Type:" "^X-Dispatcher:" "^X-Face:" "^X-Face-.*:"))

;;
;;mime, rail, wheather
;;
(load "mime-setup") ;; wl の設定ではないが、無ければ追加しておく。
;;(autoload 'wl "wl" "Wanderlust" t)
  (autoload 'wl-draft "wl" "Write draft with Wanderlust." t)

  (setq wl-thread-insert-opened t)
  (setq wl-stay-folder-window t)
;(require 'w3m-load)
  (setq rail-emulate-genjis t)
  (require 'rail)
;  (autoload 'weather-from-http "weather" "Weather" t)
;  (autoload 'weather-insert-header "weather" "Weather" t)
;  (setq weather-replace nil)


;; 色づけは最大限に
(setq font-lock-maximum-decoration t)
(setq fast-lock nil)
(setq lazy-lock nil)
(setq jit-lock t)

;;
;; カーソル行に下線を表示
;;
(setq hl-line-face 'underline)
(global-hl-line-mode t)

;;
;; 現在行に色をつける(use X11)
;;
(when (boundp 'window-system)
  ; (setq hl-line-face 'hl-line)
  (global-hl-line-mode t))

;;
;;Keylock
;;
; (load-file "~/.emacs.d/site-lisp/cleanup-evil-features.el")
(menu-bar-mode -1)
(tool-bar-mode -1)
(if window-system
  (toggle-scroll-bar -1))
(add-hook 'after-make-frame-functions
	  '(lambda (f)
	     (with-selected-frame f
	       (if window-system
		   (toggle-scroll-bar -1)))))

;;
;;popup select window
;;
(require 'popup)
(require 'popup-select-window)
(global-set-key "\C-xo" 'popup-select-window) ;;C-x o にPopup select windowをバインド
(setq popup-select-window-popup-windows 3) ;;Windowが3つ以上の時ポップアップ表示する
(setq popup-select-window-highlight-face '(:foreground "white" :background "orange")) ;;選択中のウィンドウは背景をオレンジ色にする

;;
;;color
;;
(when (boundp 'window-system)
  (custom-set-faces
   '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal :foundry "unknown"))))))

;;
;;font（X11を利用しているときのみ）
;;

;;
;;font from fukuda
;;
; まるゴシック
(when (boundp 'window-system)
    (set-face-attribute 'default nil
    			:family "Monaco"
    			:height 130)
    (set-fontset-font "fontset-default"
    		      'japanese-jisx0208
    		      '("Hiragino Maru Gothic Pro" . "iso10646-1"))
    (set-fontset-font "fontset-default"
    		      'katakana-jisx0201
    		      '("Hiragino Maru Gothic Pro" . "iso10646-1"))
    (setq face-font-rescale-alist
    	  '((".*profont-medium.*" . 1.0)
    	    (".*profont-bold.*" . 1.0)
    	    (".*nfmotoyacedar-bold.*" . 1.4)
    	    (".*nfmotoyacedar-medium.*" . 1.4)
    	    ("-cdac$" . 1.3))))
(setq initial-frame-alist default-frame-alist)

;かくゴシック
;; (when (boundp 'window-system)
;;     (set-face-attribute 'default nil
;;     			:family "Menlo"
;;     			:height 130)
;;     (set-fontset-font "fontset-default"
;;     		      'japanese-jisx0208
;;     		      '("Hiragino Kaku Gothic Pro" . "iso10646-1"))
;;     (set-fontset-font "fontset-default"
;;     		      'katakana-jisx0201
;;     		      '("Hiragino Kaku Gothic Pro" . "iso10646-1"))
;;     (setq face-font-rescale-alist
;;     	  '((".*profont-medium.*" . 1.0)
;;     	    (".*profont-bold.*" . 1.0)
;;     	    (".*nfmotoyacedar-bold.*" . 1.4)
;;     	    (".*nfmotoyacedar-medium.*" . 1.4)
;;     	    ("-cdac$" . 1.3))))
;; (setq initial-frame-alist default-frame-alist)

;Ricty Discord
; (when (boundp 'window-system)
;     (set-face-attribute 'default nil
;     			:family "Ricty Discord"
;     			:height 120)
;     (set-fontset-font "fontset-default"
;     		      'japanese-jisx0208
;     		      '("Ricty Discord" . "iso10646-1"))
;     (set-fontset-font "fontset-default"
;     		      'katakana-jisx0201
;     		      '("Ricty Discord" . "iso10646-1"))
;     (setq face-font-rescale-alist
;     	  '((".*profont-medium.*" . 1.0)
;     	    (".*profont-bold.*" . 1.0)
;     	    (".*nfmotoyacedar-bold.*" . 1.4)
;     	    (".*nfmotoyacedar-medium.*" . 1.4)
;     	    ("-cdac$" . 1.3))))
; (setq initial-frame-alist default-frame-alist)


;;
;;twitter-mode
;;
(require 'twittering-mode)
; (setq twittering-allow-insecure-server-cert t)
 (setq twittering-icon-mode t)                ; Show icons
 (setq twittering-timer-interval 90)         ; Update your timeline each 120 seconds (2 minutes)
;; o で次のURLをブラウザでオープン
(add-hook 'twittering-mode-hook
          (lambda ()
            (local-set-key (kbd "o")
               (lambda ()
                 (interactive)
                 (twittering-goto-next-uri)
                 (execute-kbd-macro (kbd "C-m"))
                 ))))
;; 表示する書式 区切り線いれたら見やすい
(setq twittering-status-format "%i %S @%s %p : %@ \n %T \n%r %R %f%L\n -------------------------------------------")
;; 起動時パスワード認証 *要 gpgコマンド
(setq twittering-use-master-password t)
;; パスワード暗号ファイル保存先変更 (デフォはホームディレクトリ)
(setq twittering-private-info-file "~/.emacs.d/twittering-mode.gpg")

;;
;;auto-install
;;
;; (require 'auto-install)
;; (setq auto-install-directory "~/.emacs.d/site-lisp/auto-install/")
;; (auto-install-update-emacswiki-package-name t)
;; (auto-install-compatibility-setup)             ; 互換性確保

;;
;; powerline.el
;;
(when matlab:use-pline
  (require 'powerline)
  (setq powerline-arrow-shape 'arrow14)  ;; フォントサイズが小さい場合
  ;; モードラインの色
  (custom-set-faces
 ;; アクティブ時
 '(mode-line ((t (:foreground "white" :background "#0044cc" :box nil))))
 ;; 非アクティブ時
 '(mode-line-inactive ((t (:foreground "white" :background "#262626" :box nil)))))
  ;; モードライン2色目
  (setq powerline-color1 "#0088cc")
  ;; モードライン3色目
  (setq powerline-color2 "white")
  ;; 背景透過
  (add-to-list 'default-frame-alist '(alpha . (0.87 0.65)))
  ;; バッファ情報の書式
  (defpowerline buffer-id (propertize (car (propertized-buffer-identification "%b"))
                                      'face (powerline-make-face color1)))
  (defpowerline row     "%l")    ; 行番号の書式
  (defpowerline column  "%c")    ; 列番号の書式
  (defpowerline percent "%p")    ; カーソル位置の割合
  (defpowerline time    "%M")    ; 時計の書式
  ;; 右部分の位置合わせ(右端から何文字分を左に寄せるか、デフォルト+15文字)
  (defun powerline-make-fill (color)
    (let ((plface (powerline-make-face color)))
      (if (eq 'right (get-scroll-bar-mode))
        (propertize " " 'display '((space :align-to (- right-fringe 36))) 'face plface)
        (propertize " " 'display '((space :align-to (- right-fringe 39))) 'face plface))))
  ;; Powerlineの書式
  (setq-default mode-line-format (list
   '("-" mode-line-mule-info mode-line-modified)
   '(:eval (concat
             (powerline-buffer-id   'left   nil powerline-color1)
             (powerline-major-mode  'left       powerline-color1)
             (powerline-minor-modes 'left       powerline-color1)
             (powerline-narrow      'left       powerline-color1 powerline-color2)
             (powerline-vc          'center                      powerline-color2)
             (powerline-make-fill                                powerline-color2)
             (powerline-row         'right      powerline-color1 powerline-color2)
             (powerline-make-text   ": "        powerline-color1)
             (powerline-column      'right      powerline-color1)
             (powerline-percent     'right      powerline-color1)
             (powerline-time        'right  nil powerline-color1)
             (powerline-make-text   "  "    nil )))))
  ;; 時計のフォーマット
  (setq display-time-string-forms '((format
    "%s/%s(%s) %s:%s" month day dayname 24-hours minutes)))
  (display-time-mode t)    ; 時計を表示
  )

;;
;; template
;;
; テンプレートのディレクトリ
(setq auto-insert-directory "~/.emacs.d/template/")

;; 各ファイルによってテンプレートを切り替える
(setq auto-insert-alist
      (nconc '(
               ("\\.c$" . ["template.c" my-template])
               ("\\.h$"   . ["template.h" my-template])
               ) auto-insert-alist))
(require 'cl)
(defvar template-replacements-alists
  '(("%file%"             . (lambda () (file-name-nondirectory (buffer-file-name))))
    ("%file-without-ext%" . (lambda () (file-name-sans-extension (file-name-nondirectory (buffer-file-name)))))
    ("%include-guard%"    . (lambda () (format "INCLUDED_%s" (upcase (file-name-sans-extension (file-name-nondirectory buffer-file-name))))))))

(defun my-template ()
  (time-stamp)
  (mapc #'(lambda(c)
        (progn
          (goto-char (point-min))
          (replace-string (car c) (funcall (cdr c)) nil)))
    template-replacements-alists)
  (goto-char (point-max))
  (message "done."))
(add-hook 'find-file-not-found-hooks 'auto-insert)

;;;
;;; setting for aspell
;;;
(setq-default ispell-program-name "aspell")
(eval-after-load "ispell"
 '(add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))
